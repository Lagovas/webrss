from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.core.urlresolvers import reverse_lazy
from webrss.apps.webrss.views import (LoginView, IndexView, SetReadedView,
                                      SetSavedView, SetTagView, FeedView,
                                      AddFeed, MarkRead, RemoveFeed, SavedView)
from webrss.apps.webrss.models import Feed, FeedItem

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login',
        kwargs={'login_url': reverse_lazy('login')}, name='logout'),
    url(r'^set-readed/(?P<id>\d+?)/$', SetReadedView.as_view(), name='set-readed'),
    url(r'^set-saved/(?P<id>\d+?)/$', SetSavedView.as_view(), name='set-saved'),
    url(r'^set-tag/(?P<id>\d+?)/$', SetTagView.as_view(), name='set-tag'),
    url(r'^feeds/(?P<slug>[\w-]+?)/$', FeedView.as_view(), name='feed'),
    url(r'^add-feed/$', AddFeed.as_view(), name='add-feed'),
    url(r'^mark-read/$', MarkRead.as_view(), name='mark-read'),
    url(r'^remove-feed/$', RemoveFeed.as_view(), name='remove-feed'),
    url(r'^saved/$', SavedView.as_view(), name='saved'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^i18n/', include('django.conf.urls.i18n')),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )