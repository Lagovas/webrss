import json
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.views.generic import View, FormView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login
from django import forms
from webrss.apps.webrss.models import Feed, FeedItem, Tag
from webrss.apps.webrss.forms import FeedItemForm, TagForm, FeedForm

class LOGIN_FORM_TYPE:
    LOGIN = 0
    REGISTER = 1

class LoginView(View):
    template_name = "login.html"
    def register(self):
        form = UserCreationForm(self.request.POST)
        if form.is_valid():
            form.save()
            username = form.clean_username()
            password = form.clean_password2()
            user = authenticate(username=username, password=password)
            login(self.request, user)
            return HttpResponseRedirect(reverse_lazy('index'))

    def login(self):
        form = AuthenticationForm(self.request.POST)
        if form.is_valid():
            user = form.get_user()
            login(self.request, user)
            return HttpResponseRedirect(reverse_lazy('index'))

    def post(self, *args, **kwargs):
        type = self.request.POST.get('type', '')
        if type:
            if type==LOGIN_FORM_TYPE.LOGIN:
                return self.login()
            elif type==LOGIN_FORM_TYPE.REGISTER:
                return self.register()
            else:
                return HttpResponseRedirect(reverse_lazy('login'))
        else:
            return HttpResponseRedirect(reverse_lazy('login'))


    def get(self, *args, **kwargs):
        return render(self.request, self.template_name, {'login_form': AuthenticationForm, 'register_form': UserCreationForm})

class IndexView(View):
    template_name = 'index.html'

    @classmethod
    def get_context(cls, user):
        return {'items': FeedItem.get_unreaded(user),
                'feeds': Feed.objects.filter(user=user),
                }

    def get(self, *args, **kwargs):
        context = self.get_context(self.request.user)
        return render(self.request, self.template_name, context, context_instance=RequestContext(self.request, {}))

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)

class SetReadedView(View):
    form_class = FeedItemForm
    def update_feed_item(self, feed_item):
        feed_item.is_readed = True
    def get_response(self, feed_item):
        return HttpResponse()
    def post(self, *args, **kwargs):
        if self.request.is_ajax():
            form = self.form_class({'id': self.kwargs['id']})
            form.is_valid()
            if 'id' in form.cleaned_data:
                feed_item = form.cleaned_data['id']
                self.update_feed_item(feed_item)
                feed_item.save()
                return self.get_response(feed_item)
        else:
            raise Http404

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(SetReadedView, self).dispatch(*args, **kwargs)

class SetSavedView(SetReadedView):
    def update_feed_item(self, feed_item):
        feed_item.is_saved = not feed_item.is_saved
    def get_response(self, feed_item):
        return HttpResponse(json.dumps({'result': feed_item.is_saved}), content_type="application/json")


class SetTagView(View):
    def post(self, *args, **kwargs):
        if self.request.is_ajax():
            tag = Tag.objects.get_or_create(name=self.request.POST['name'],
                                            user=self.request.user)[0]
            feed_item = forms.ModelChoiceField(
                queryset=FeedItem.objects.all()).clean(kwargs['id'])
            if feed_item:
                feed_item.tags.add(tag.pk)
                return HttpResponse()
            else:
                raise Http404
        else:
            raise Http404
    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(SetTagView, self).dispatch(*args, **kwargs)

class FeedView(View):
    template_name = 'feeds.html'

    def get_items(self):
        return Feed.objects.get(slug=self.kwargs['slug']).feeditem_set.all()

    def get(self, *args, **kwargs):
        try:
            feed = Feed.objects.get(slug=kwargs['slug'])
        except Feed.DoesNotExist:
            raise Http404

        context = {'items': self.get_items(),
                   'feed': feed,
                   'feeds': Feed.objects.filter(user=self.request.user)}
        return render(self.request, self.template_name, context)

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(FeedView, self).dispatch(*args, **kwargs)

class AddFeed(View):
    form_class = FeedForm

    def post(self, *args, **kwargs):
        data = self.request.POST.copy()
        data['user'] = self.request.user.pk
        form = self.form_class(data)
        if form.is_valid():
            feed = form.save()
            feed.load_items()
            return HttpResponseRedirect(reverse_lazy('index'))
        else:
            context = IndexView.get_context(self.request.user)
            context['feed_form'] = form #replace form to form with error messages
            return render(self.request, IndexView.template_name, context)

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(AddFeed, self).dispatch(*args, **kwargs)

class MarkRead(View):
    def post(self, *args, **kwargs):
        FeedItem.objects.filter(feed__user=self.request.user, is_readed=False).update(is_readed=True)
        return HttpResponseRedirect(reverse_lazy('index'))

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(MarkRead, self).dispatch(*args, **kwargs)

class RemoveFeed(View):
    form_class = FeedForm
    def post(self, *args, **kwargs):
        form = self.form_class(self.request.POST)
        form.is_valid()
        if 'id' in form.cleaned_data:
            form.cleaned_data['id'].delete()
            return HttpResponse()
        else:
            raise Http404

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(RemoveFeed, self).dispatch(*args, **kwargs)


class SavedView(FeedView):
    template_name = 'saved.html'
    def get_items(self):
        return FeedItem.objects.filter(feed__user=self.request.user, is_saved=True)

    def get(self, *args, **kwargs):
        return render(self.request, self.template_name, {'items': self.get_items(),
                                                         'feeds': Feed.objects.filter(user=self.request.user)})
