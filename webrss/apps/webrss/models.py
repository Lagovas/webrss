import time
import datetime
from django.core import serializers
from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import User
import autoslug
from transliterate import slugify
import feedparser
from django.utils.translation import ugettext_lazy as _

class Tag(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=25, unique=True, verbose_name=_('Tag name'))
    def __unicode__(self):
        return self.name

class Feed(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Feed name'))
    url = models.URLField(verbose_name=_('Url'))
    user = models.ForeignKey(User)
    slug = autoslug.AutoSlugField(
        populate_from='name', slugify=lambda value: slugify(value, 'ru'))

    def get_absolute_url(self):
        return reverse('feed', kwargs={'slug': self.slug})

    @classmethod
    def serialize(cls, user):
        return serializers.serialize('xml', cls.objects.filter(user=user))

    def load_items(self):
        feeditem = FeedItem.objects.filter(feed=self).order_by('-datetime').first()
        if feeditem:
            last_datetime_feeditem = feeditem.datetime
            seconds_since_epoch = time.mktime(last_datetime_feeditem.timetuple()) * 1000
            data = feedparser.parse(self.url, modified=time.gmtime(seconds_since_epoch))
        else:
            data = feedparser.parse(self.url)
        for item in data.entries:
            feed_item = FeedItem()
            feed_item.title = item.title
            feed_item.description = item.description
            feed_item.link = item.link
            feed_item.datetime = datetime.datetime(*(item.published_parsed[0:6]))
            feed_item.feed = self
            feed_item.save()

    def __unicode__(self):
        return self.name

class FeedItem(models.Model):
    title = models.CharField(max_length=255, verbose_name=_('Title'))
    description = models.TextField(verbose_name=_('Description'))
    link = models.URLField()
    datetime = models.DateTimeField()
    feed = models.ForeignKey(Feed)
    is_readed = models.BooleanField(default=False)
    is_saved = models.BooleanField(default=False)
    slug = autoslug.AutoSlugField(
        populate_from='title', slugify=lambda value: slugify(value, 'ru'))
    tags = models.ManyToManyField(Tag, blank=True)

    @classmethod
    def serialize(cls, user):
        return serializers.serialize(
            'xml', cls.objects.filter(user=user, is_saved=True))

    @classmethod
    def get_unreaded(cls, user):
        items = FeedItem.objects.filter(
            is_readed=False, feed__user=user).order_by('feed')
        return cls._to_dict_by_feed(items)

    @classmethod
    def get_saved(cls, user):
        items = FeedItem.objects.filter(
            is_saved=True, feed__user=user).order_by('feed')
        return cls._to_dict_by_feed(items)

    @classmethod
    def _to_dict_by_feed(cls, qs):
        output = {}
        for item in qs.iterator():
            key = item.feed.name
            if key in output:
                output[key].append(item)
            else:
                output[key] = [item, ]
        return output

    @classmethod
    def get_by_tag(cls, tag_name):
        try:
            return Tag.objects.get(name=tag_name).feeditem_set.all()
        except Tag.DoesNotExist:
            return None

    def __unicode__(self):
        return self.title