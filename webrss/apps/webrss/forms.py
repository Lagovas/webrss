__author__ = 'Lagovas'

from django import forms
from webrss.apps.webrss.models import FeedItem, Feed, Tag

class FeedItemForm(forms.ModelForm):
    id = forms.ModelChoiceField(queryset=FeedItem.objects.all(), required=True)

    class Meta:
        localized_fields = ('description', 'title', 'date')
        model = FeedItem

class FeedForm(forms.ModelForm):
    id = forms.ModelChoiceField(queryset=Feed.objects.all(), required=False)
    class Meta:
        model = Feed
        localized_fields = ('__all__')

class TagForm(forms.ModelForm):
    id = forms.ModelChoiceField(queryset=Tag.objects.all(), required=True)

    class Meta:
        localized_fields = ('__all__')
        model = Tag
