from django.contrib import admin
from webrss.apps.webrss.models import FeedItem, Feed, Tag

admin.site.register(Feed)
admin.site.register(FeedItem)
admin.site.register(Tag)
