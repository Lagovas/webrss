import time
import datetime as dt
from webrss.apps.webrss.models import FeedItem, Feed
__author__ = 'lagovas'

class FeedParseMiddleware(object):
    #_seconds_in_hour = 60*60
    _seconds_in_hour = 1

    def _get_new_items(self, last_item_datetime, request):
        if (dt.datetime.now(last_item_datetime.tzinfo) - last_item_datetime).seconds > self._seconds_in_hour:
            print 'feed parse'
            request.session['last_datetime'] = dt.datetime.now(last_item_datetime.tzinfo)
            print request.session.items()
            for feed in Feed.objects.filter(user=request.user):
                feed.load_items()

    def process_request(self, request):
        t1 = time.time()
        if request.user.is_authenticated():
            print request.session.items()
            last_datetime = request.session.get('last_datetime', None)
            if last_datetime:
                print 'session'
                self._get_new_items(last_datetime, request)
            else:
                print 'from db'
                last_item = FeedItem.objects.filter().order_by('-datetime').values_list('datetime', flat=True)[:1].first()
                print FeedItem.objects.filter().order_by('-datetime').values_list('datetime', flat=True)[:1].query
                if last_item:
                    print last_item
                    last_item_datetime = last_item
                    self._get_new_items(last_item_datetime, request)
        print time.time()-t1
        return None
