function set_readed(){
    id = this.parentNode.id.substr(5);
    $.ajax({
        url: '/set-readed/'+id+'/',
        type: "POST",
        cache: false,
        error: function(){alert('Something was wrong');},
        success: function(){
            $('#read_'+id).addClass('text-muted');
    }});
    //this.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.className = '';
    next = this;
    while (next.className != "hidden"){
        next = next.nextSibling;
    }
    next.className = '';
}

function set_saved(){
    id = this.id.substr(5);
    $.ajax({
        url: '/set-saved/'+id+'/',
        type: "POST",
        cache: false,
        dataType: "json",
        error: function(){alert('Something was wrong');},
        success: function(data){
            if(data.result){
                $('#save_'+id)[0].className = "glyphicon glyphicon-floppy-saved set-saved";
            } else {
                $('#save_'+id)[0].className = "glyphicon glyphicon-floppy-disk set-saved";
            }
    }});
}

function set_tag(){
    id = this.id.substr(4);
    $( "#new-tag-dialog" ).removeClass('hidden');
    $( "#new-tag-dialog" ).dialog("open");
}

function remove_feed_handler(){
    var feed_id = this.id.substr(12);
    window.remove_feed = this;

    $.ajax({
        url: '/remove-feed/',
        type: "POST",
        data: {id: feed_id},
        cache: false,
        dataType: "json",
        success: function(data){
            remove_feed = window.remove_feed;
            remove_feed = remove_feed.parentNode;
            var parent = remove_feed.parentNode;
            parent.removeChild(remove_feed);
        }
    });
}

$(document).ready(function(){
    function add_tag_handler(){
        var tag_name = $('#set-tag-form>input')[1].value;//input after csrf
        $.ajax({
            url: '/set-tag/'+id+'/',
            type: "POST",
            cache: false,
            dataType: "json",
            data: {name: tag_name},
            error: function(){$('#tag_'+id)[0].className = "glyphicon glyphicon-tag set-tag";},
            success: function(data){
                var tag = $('#tag_'+id)[0];
                while(tag.className != "tags"){
                    tag = tag.nextSibling;
                }
                tag.textContent = tag.textContent + " " + $('#set-tag-form>input')[1].value;
            }
        });
        $("#new-tag-dialog").dialog("close");
    }
    $('.set-readed').on('click', set_readed);
    $('.set-saved').on('click', set_saved);
    $('.set-tag').on('click', set_tag);
    $("#new-tag-dialog").dialog({
        resizable: false,
        height:160,
        width:200,
        modal: true,
        autoOpen: false
    });
    $('#set-tag-btn').on('click', add_tag_handler);
    $('.glyphicon-remove').on('click', remove_feed_handler);
});