from webrss.apps.webrss.forms import FeedForm, TagForm
__author__ = 'lagovas'


def base(request):
    return {
        'feed_form': FeedForm,
        'tag_form': TagForm(),
        'user': request.user}

